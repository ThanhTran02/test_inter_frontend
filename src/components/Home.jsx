import React from 'react'
import CardProducts from './CardProducts'
import YourCart from './YourCart'
import { styled } from 'styled-components'

export const Home = () => {
  return (
    <Div className='flex flex-col items-center justify-center lg:w-full  lg:h-full '>
      <div className='flex flex-col  lg:flex-row w-full lg:w-1/2 lg:justify-between lg:items-center gap-8'>
        <CardProducts/>
        <YourCart/>
      </div>
    </Div>
  )
}

export default Home

const Div = styled.div`
 &::before {
    content: "";
    display: block;
    position: fixed;
    width: 300%;
    height: 100%;
    top: 50%;
    left: 50%;
    border-radius: 100%;
    transform: translateX(-50%) skewY(-8deg);
    background-color: #F6C90E;
    z-index: -1;
    animation: wave 8s ease-in-out infinite alternate;
  }`

  