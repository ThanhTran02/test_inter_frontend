import React, { useDebugValue, useReducer } from "react";
import { useDispatch, useSelector } from "react-redux";
import { styled } from "styled-components";
import { manageProductActions } from "../store/manageProduct/slice";

export const CardProducts = () => {
  const { shoes } = useSelector((state) => state.manageProduct);
  const dispatch = useDispatch();

  return (
    <Container className="w-full  lg:h-[450px] bg-white rounded-[28px] overflow-hidden z-0">
      <div className="z-10 bg-white">
        <div className="flex items-start gap-1 flex-col px-5 py-2 ">
          <img
            src="./img/nike.png"
            alt="logo-nike"
            className="object-contain  h-5 lg:h-7 z-10"
          />
          <h2 className="font-extrabold  font-sans text-[16px] lg:text-xl  text-start z-10">
            Our Products
          </h2>
        </div>
        <div className="h-[380px] card z-10">
          <div className="px-5 py-2 z-10 ">
            {shoes.map((item) => (
              <div
                key={item.id}
                className="flex flex-col gap-4 items-start mb-14"
              >
                <div
                  className={`bg-[${item.color}] rounded-[28px] h-[300px] flex flex-col items-center justify-center z-10`}
                >
                  <img
                    src={item.image}
                    alt=""
                    className="object-cover transform -rotate-[20deg] -translate-x-3 z-10"
                  />
                </div>
                <p className="font-extrabold text-[14px] lg:text-[18px] z-10">
                  {item.name}
                </p>
                <h3 className="text-start font-normal text-[12px] lg:text-[15px] text-[#838383] z-10">
                  {item.description}
                </h3>
                <div className="flex justify-between items-center w-full  z-10">
                  <p className="font-extrabold">{item.price}</p>
                  {item.choosed === 1 ? (
                    <button
                      className="bg-[#ffcd00] rounded-full py-1 px-2 lg:py-2 lg:px-3 font-bold text-[12px] lg:text-[14px] box"
                      onClick={() => {
                        dispatch(
                          manageProductActions.setProduct({ ...item, amout: 1 })
                        );
                        dispatch(manageProductActions.setChoosedProduct(item));
                      }}
                    >
                      ADD TO CART
                    </button>
                  ) : (
                    <button className="bg-[#ffcd00] rounded-full items-center flex justify-center h-8 w-8 box">
                      <img
                        src="./img/check.png"
                        alt="check"
                        className="object-contain w-3/5"
                      />
                    </button>
                  )}
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
    </Container>
  );
};

export default CardProducts;
const Container = styled.div`
position: relative;
  &::before {
    content: "";
    display: block;
    position: absolute;
    width: 250px;
    height: 250px;
    border-radius: 100%;
    background-color: #F6C90E;
    top: -20%;
    left: -50%;
    z-index: 0;
  }
  .card {
    z-index:10;
    overflow: auto;
    overflow: -moz-scrollbars-none;
    -ms-overflow-style: none;
    &::-webkit-scrollbar {
      width: 0 !important;
      display: none;
    }
  }
  .box{
    box-shadow: 0 3px 6px rgba(0, 0, 0, 0.2);
  }
`;
