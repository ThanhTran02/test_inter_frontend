import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { styled } from "styled-components";
import { manageProductActions } from "../store/manageProduct/slice";

export const YourCart = () => {
  const { cart, bill } = useSelector((state) => state.manageProduct);
  const dispatch = useDispatch();
  return (
    <Container className="w-full h-[450px] bg-white rounded-[28px] overflow-hidden z-0">
      <div>
        <div className="flex items-start gap-1 flex-col px-5 py-2 z-10">
          <img
            src="./img/nike.png"
            alt="logo-nike"
            className="object-contain h-5 lg:h-7 z-10"
          />
          <div className="flex justify-between items-center w-full z-10">
            <h2 className="font-extrabold  font-sans   text-start text-[16px] lg:text-xl z-10">
              Your cart
            </h2>
            <p className="font-bold">{`$${bill}`}</p>
          </div>
        </div>
        <div className="h-[380px] card z-10">
          <div className="px-5 py-2 z-10 ">
            {cart.length !== 0 ? (
              cart.map((item) => (
                <div
                  key={item.id}
                  className="grid grid-cols-3 items-center mb-10 z-10 "
                >
                  <div
                    className={`bg-[${item.color}] rounded-full h-16 w-16 flex flex-col items-center justify-center z-10`}
                  >
                    <img
                      src={item.image}
                      alt=""
                      className="scale-[1.35]  -translate-y-3 transform -rotate-[20deg] z-10"
                    />
                  </div>
                  <div className="col-span-2 flex flex-col gap-2">
                    <p className="font-bold text-[13px] text-start z-10">
                      {item.name}
                    </p>
                    <p className="font-extrabold  text-[15px] lg:text-[17px] text-start z-10">
                      {item.price}
                    </p>
                    <div className="flex w-full justify-between items-center z-10">
                      <div className="flex gap-2 items-center justify-start">
                        <button className="bg-[#e1e4e7] rounded-full items-center flex justify-center h-7 w-7 z-10 ">
                          {" "}
                          <img
                            src="./img/minus.png"
                            alt="-"
                            className="object-contain w-3/5"
                            onClick={() =>
                              dispatch(
                                manageProductActions.editAmout({
                                  id: item.id,
                                  quantity: -1,
                                })
                              )
                            }
                          />
                        </button>
                        <p>{item.amout}</p>
                        <button
                          className="bg-[#e1e4e7] rounded-full items-center flex justify-center h-7 w-7 z-10"
                          onClick={() =>
                            dispatch(
                              manageProductActions.editAmout({
                                id: item.id,
                                quantity: 1,
                              })
                            )
                          }
                        >
                          {" "}
                          <img
                            src="./img/plus.png"
                            alt="+"
                            className="object-contain w-3/5"
                          />
                        </button>
                      </div>
                      <button
                        className="bg-[#ffcd00] rounded-full items-center flex justify-center h-7 w-7"
                        onClick={() => {
                          dispatch(
                            manageProductActions.removeProduct({
                              id: item.id,
                              price: item.price,
                              amout: item.amout,
                            })
                          );
                          dispatch(
                            manageProductActions.setChoosedProduct(item)
                          );
                        }}
                      >
                        {" "}
                        <img
                          src="./img/trash.png"
                          alt="delete"
                          className="object-contain w-3/5"
                        />
                      </button>
                      <div></div>
                    </div>
                  </div>
                </div>
              ))
            ) : (
              <div className="z-10 relative">
                <p className="text-start text-[14px] lg:text-[14px] text-[#777777] z-10 ">
                  Your cart is empty
                </p>
              </div>
            )}
          </div>
        </div>
      </div>
    </Container>
  );
};

export default YourCart;
const Container = styled.div`
  position: relative;
  &::before {
    content: "";
    display: block;
    position: absolute;
    width: 250px;
    height: 250px;
    border-radius: 100%;
    background-color: #f6c90e;
    top: -20%;
    left: -50%;
    z-index: 0;
  }
  .card {
    z-index: 10;
    overflow: auto;
    overflow: -moz-scrollbars-none;
    -ms-overflow-style: none;
    &::-webkit-scrollbar {
      width: 0 !important;
      display: none;
    }
  }
`;
