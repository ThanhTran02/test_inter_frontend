import { combineReducers } from "@reduxjs/toolkit";
import { manageProductReducer } from "./manageProduct/slice";

export const rootReducer = combineReducers({
    manageProduct:manageProductReducer,
})